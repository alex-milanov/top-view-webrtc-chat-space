# prep dirs
rm -rf dist
mkdir dist dist/js dist/css dist/assets
# index
node_modules/.bin/pug --pretty < src/pug/index.pug > dist/index.html
# assets
node bin/move-assets.js
# js
node_modules/.bin/browserify src/js/index.js -o dist/js/app.js --debug
# sass
sh bin/build-sass.sh
