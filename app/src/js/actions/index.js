'use strict';

const Rx = require('rx');
const $ = Rx.Observable;

const {obj, fn, arr} = require('iblokz-data');

// const switchFn = (value, options) => options[value] || options['default'] || false;

const initial = {
	player: {
		skin: 'player',
		name: 'Player 1',
		position: {
			x: 0,
			y: 0
		},
		direction: 'right',
		status: 'idle',
		frame: 0,
		force: 0
	}
};

const move = (direction, force) =>
	state => obj.patch(state, 'player', {
		force,
		direction: direction !== '' ? direction : state.player.direction
	});

const jump = () => state => obj.patch(state, ['player', 'status'],
	['jumping', 'falling'].indexOf(state.player.status) === -1
		? 'jumping'
		: state.player.status
);

const collides = (oldPos, newPos, obstacles) =>
	({x: false, y: false});

const calculatePos = player => ({
	x: player.position.x + (player.direction.match('right|left')
		? player.force *
			((player.direction.match('right')) ? 1 : -1)
		: 0),
	y: player.position.y + (player.direction.match('up|down')
		? player.force *
			((player.direction.match('up')) ? 1 : -1)
		: 0)
});

const tick = () =>
	state => obj.patch(state, 'player',
		fn.pipe(
			() => calculatePos(state.player),
			newPos => ({
				newPos,
				collides: collides(state.player.position, newPos, state.boxes)
			}),
			// data => ((data.collides.x || data.collides.y) && console.log(data), data),
			({newPos, collides}) => ({
				position: {
					x: newPos.x,
					y: newPos.y
				},
				status: obj.switch(state.player.status, {
					'default':
						state.player.force === 4
							? 'running'
							: state.player.force === 3
								? 'walking'
								: 'idle'
				}),
				frame: 0
			})
		)()
	);

// actions
const set = (key, value) => state => obj.patch(state, key, value);
const toggle = key => state => obj.patch(state, key, !obj.sub(state, key));
const arrToggle = (key, value) => state =>
	obj.patch(state, key,
		arr.toggle(obj.sub(state, key), value)
	);

module.exports = {
	initial,
	set,
	toggle,
	arrToggle,
	move,
	jump,
	tick
};
