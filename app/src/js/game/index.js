
const ui = require('./ui');

const {section, span} = require('iblokz-snabbdom-helpers');

module.exports = ({actions, state}) => section('#game', [].concat(
	ui({state, actions}),
	section('#player', {
		style: {
			left: `${256 + state.player.position.x}px`,
			bottom: `${256 + state.player.position.y}px`,
			backgroundImage: `url("assets/${state.player.skin}.png")`
		},
		class: {
			left: state.player.direction.match('left'),
			[state.player.status]: state.player.status
		}
	}, [
		span('.player-name', state.player.name)
	]),
	section('.wall', {style: {
		top: `64px`, left: `192px`, height: `64px`, width: `512px`}}),
	section('.wall.vertical.left', {style: {
		top: `64px`, left: `128px`, height: `${128 + 512}px`, width: `64px`}}),
	section('.floor', {style: {
		top: `${128}px`, left: `192px`, height: `512px`, width: `512px`}}),
	section('.wall', {style: {
		top: `${128 + 512}px`, left: `192px`, height: `64px`, width: `512px`}}),
	section('.wall.vertical.right', {style: {
		top: `64px`, left: `${128 + 512 + 64}px`, height: `${128 + 512}px`, width: `64px`}})
	// state.boxes.map(({left, bottom}) =>
	// 	section('.box', {
	// 		style: {
	// 			left,
	// 			bottom: bottom + 128
	// 		}
	// 	})
	// )
));
