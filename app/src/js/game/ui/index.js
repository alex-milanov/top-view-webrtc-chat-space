
const {
	section, img, div, span,
	form, input, button, label
} = require('iblokz-snabbdom-helpers');

module.exports = ({actions, state}) => section('#ui', [
	form({on: {submit: ev => ev.preventDefault()}}, [
		div([
			label('Name: '),
			input(`[name="name"]`, {
				props: {value: state.player.name},
				on: {change: ev => actions.set(['player', 'name'], ev.target.value)}
			})
		]),
		div([].concat(
			label('Skin: '),
			['player', 'player-2', 'player-3'].map(skin =>
				img(`[src="assets/${skin}-avatar.png"]`, {
					class: {selected: state.player.skin === skin},
					on: {click: () => actions.set(['player', 'skin'], skin)}
				})
			)
		))
	])
]);
